# DashMultiPageApp example package

## Install deps

Using Julia 1.6+,

```
$ julia -e 'import Pkg; Pkg.activate("."); Pkg.instantiate()'
```

## Run the app

```
$ julia bin/run.jl

# or
$ julia
julia> include("bin/run.jl")

# and open up localhost:8002/app/
```

## Dev the app

### "Regular" Dash.jl debug mode

```
$ julia bin/dev.jl
# open up localhost:9002
```

which does not work from the REPL.

### Experimental Revise-based REPL watcher

```
$ julia
pkg> activate bin/env/devrepl
pkg> dev .
julia> include("bin/devrepl.jl");

# open up localhost:9002
```

which should work with
[`Infiltrator.jl`](https://github.com/JuliaDebug/Infiltrator.jl) (tested with
its `v1.0.3` version), allow us to debug callbacks and handlers using
`@infiltrate` breakpoints.

Note that debugging the app with
[`Debugger.jl`](https://github.com/JuliaDebug/Debugger.jl) won't work as it
can't set breakpoints inside async tasks yet
([ref](https://github.com/JuliaDebug/JuliaInterpreter.jl/issues/413)).

## Run the tests

```
$ julia
(DashMultiPageApp) pkg> test

# more granularly
pkg> activate test
julia> include("test/app/runtests/jl");
julia> include("test/callbacks/runtests/jl");
julia> include("test/handlers/runtests/jl");
julia> include("test/style/runtests/jl");
```

## Directory tree

<details>
<summary>click here to see</summary>

```
$ tree -a
.
├── bin
│   ├── dev.jl
│   ├── devrepl.jl
│   ├── env
│   │   └── devrepl
│   │       ├── Manifest.toml
│   │       └── Project.toml
│   ├── lint.jl
│   ├── run.jl
│   └── _watchapp.jl
├── .gitignore
├── .JuliaFormatter.toml
├── LICENSE
├── Manifest.toml
├── Project.toml
├── README.md
├── src
│   ├── app.jl
│   ├── DashMultiPageApp.jl
│   ├── dataset.jl
│   ├── logging.jl
│   └── pages
│       ├── page_home
│       │   └── layouts.jl
│       ├── page_scatter
│       │   ├── callbacks.jl
│       │   ├── handlers.jl
│       │   └── layouts.jl
│       ├── pages.jl
│       └── page_splom
│           ├── callbacks.jl
│           ├── handlers.jl
│           └── layouts.jl
└── test
    ├── app
    │   └── runtests.jl
    ├── callbacks
    │   ├── page_scatter.jl
    │   ├── page_splom.jl
    │   └── runtests.jl
    ├── handlers
    │   ├── page_scatter.jl
    │   ├── page_splom.jl
    │   └── runtests.jl
    ├── Manifest.toml
    ├── Project.toml
    ├── runtests.jl
    └── style
        └── runtests.jl

13 directories, 36 files
```

</details>

## Code style

[YAS](https://github.com/jrevels/YASGuide), checked using
[`JuliaFormatter.jl`](https://github.com/domluna/JuliaFormatter.jl).

### Lint with

```
$ julia bin/lint.jl
```

Additional package syntax tests done via
[`Aqua.jl`](https://github.com/JuliaTesting/Aqua.jl).

## License

[MIT](./LICENSE) - etpinard
