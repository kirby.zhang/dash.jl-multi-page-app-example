function addcallbacks!(app::Dash.DashApp, pg::AbstractPageSplom)
    pre = prefix(pg)

    callback!(app,
              Output("$pre-pts", "data"),
              Input("content", "children"),
              State("url", "href");
              prevent_initial_call=false) do _, href
        @cbinfo "=>pts" href
        return parsehref(pg, href)
    end

    callback!(app,
              Output("$pre-graph", "figure"),
              Output("$pre-table", "data"),
              Output("$pre-query", "data"),
              Input("$pre-pts", "data"),
              Input("$pre-graph", "selectedData"),
              Input("$pre-btn-clear", "n_clicks")) do pts, seldata, _
        @cbinfo "=>" triggered(pg) pts typeof(seldata)
        pts = if triggered(pg) == "$pre-pts.data"
            pts
        elseif triggered(pg) == "$pre-btn-clear.n_clicks"
            []
        else
            parseseldata(pg, seldata)
        end
        @cbinfo "=>" pts
        return (makefig(pg, pts),
                maketabledata(pg, pts),
                makequery(pg, pts))
    end

    addcallback_query!(app, pg)

    return app
end
