function makelayout(pg::AbstractPageSplom)
    pre = prefix(pg)
    return html_div([dcc_store(; id="$pre-pts"),
                     makelayout_backbtn(pg),
                     html_h1("Page Splom"),
                     html_br(),
                     html_div([html_div([html_button("Clear selections";
                                                     id="$pre-btn-clear")];
                                        className="row"),
                               html_div(makelayout_graph(pg)),
                               dash_datatable(; id="$pre-table",
                                              page_size=20,
                                              columns=[(name=x, id=x)
                                                       for x in data_names()])];
                              className="container"),
                     makelayout_query(pg)])
end

function makelayout_graph(pg::AbstractPageSplom)
    modeBarButtons = [["toImage", "zoom2d", "pan2d", "lasso2d", "autoScale2d"]]
    return makelayout_graph(pg; modeBarButtons)
end
