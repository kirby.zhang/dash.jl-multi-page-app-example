const k_pts = "pts"

function parsehref(::AbstractPageSplom, href::String)
    params = try
        queryparams(URI(href))
    catch e
        Dict()
    end
    return parseopt(params, k_pts, [])
end

function parseseldata(::AbstractPageSplom, seldata)
    pts = map(_ -> Int[], data_species())
    for pt in get(seldata, :points, [])
        if haskey(pt, :curveNumber)
            push!(pts[pt.curveNumber + 1], get(pt, :pointNumber, -1))
        end
    end
    return pts
end
parseseldata(::AbstractPageSplom, seldata::Nothing) = []

function makefig(pg::AbstractPageSplom, pts)
    traces = Any[]
    for (i, name) in enumerate(data_species())
        d = DATA[DATA.Species .== name, :]
        dimensions = [(label,
                       values=d[!, Symbol(label)])
                      for label in data_variables()]
        selectedpoints = isempty(pts) ? nothing : pts[i]
        push!(traces,
              (; type="splom",
               marker=(size=pg.markersize,),
               diagonal=(visible=false,),
               showupperhalf=false,
               dimensions,
               selectedpoints,
               name))
    end
    layout = (dragmode="lasso",)
    return (; data=traces, layout)
end

function maketabledata(::AbstractPageSplom, pts)
    tabledata = Any[]
    if !isempty(pts)
        for (i, name) in enumerate(data_species())
            d = DATA[DATA.Species .== name, :]
            for j in pts[i]
                row = d[j + 1, :]
                push!(tabledata, Dict(names(row) .=> string.(values(row))))
            end
        end
    end
    return tabledata
end

# PageSplom dispatch that wraps the general `makequery`
function makequery(::AbstractPageSplom, pts)
    return isempty(pts) ? "" : makequery(k_pts => pts)
end
