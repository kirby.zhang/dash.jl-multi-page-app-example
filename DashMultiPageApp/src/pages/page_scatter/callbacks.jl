function addcallbacks!(app::Dash.DashApp, pg::AbstractPageScatter)
    pre = prefix(pg)

    callback!(app,
              Output("$pre-cl-species", "value"),
              Output("$pre-dd-xvar", "value"),
              Output("$pre-dd-yvar", "value"),
              Input("content", "children"),
              State("url", "href");
              prevent_initial_call=false) do _, href
        @cbinfo "=>values" href
        return parsehref(pg, href)
    end

    callback!(app,
              Output("$pre-graph", "figure"),
              Output("$pre-query", "data"),
              Input("$pre-cl-species", "value"),
              Input("$pre-dd-xvar", "value"),
              Input("$pre-dd-yvar", "value")) do species, xvar, yvar
        @cbinfo "=>graph.figure,url.search" species xvar yvar
        return (makefig(pg, species, xvar, yvar),
                makequery(pg, species, xvar, yvar))
    end

    addcallback_query!(app, pg)

    return app
end

function addcallback_query!(app::Dash.DashApp, pg::AbstractPage)
    pre = prefix(pg)
    jsfun = """
function $(pre)_updateSearch (query) {
    if (query !== false) {
        const loc = window.location
        const path = loc.protocol + "//" + loc.host + loc.pathname + query
        window.history.pushState({path: path}, "", path)
    }
    return false
}
"""
    callback!(jsfun,
              app,
              Output("$pre-query-dummy", "data"),
              Input("$pre-query", "data"))
    return app
end
