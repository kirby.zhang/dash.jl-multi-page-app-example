function makelayout(pg::AbstractPageScatter)
    pre = prefix(pg)

    ddcommonopts = (options=[(label=x, value=x) for x in data_variables()],
                    searchable=true,
                    clearable=false,
                    className="three columns")
    widgetrow = [dcc_checklist(; id="$pre-cl-species",
                               options=[(label=x, value=x) for x in data_species()],
                               labelStyle=(display="inline-block",),
                               className="four columns"),
                 dcc_dropdown(; id="$pre-dd-xvar",
                              placeholder="X variable?",
                              ddcommonopts...),
                 dcc_dropdown(; id="$pre-dd-yvar",
                              placeholder="Y variable?",
                              ddcommonopts...)]

    return html_div([makelayout_backbtn(pg),
                     html_h1("Page Scatter"),
                     html_br(),
                     html_div([html_div(widgetrow; className="row"),
                               html_div(makelayout_graph(pg))];
                              className="container"),
                     makelayout_query(pg)])
end

# example: create methods that all `AbstractPage` subtypes could use
function makelayout_graph(pg::AbstractPage; extraconfig...)
    pre = prefix(pg)
    toImageButtonOptions = (filename=pg.route,
                            width=nothing,
                            height=nothing)
    displaylogo = false
    return dcc_graph(; id="$pre-graph",
                     config=(; toImageButtonOptions,
                             displaylogo,
                             extraconfig...))
end

function makelayout_query(pg::AbstractPage)
    pre = prefix(pg)
    # store resulting search query
    # to pass its value to clientside callback
    # which outputs gibberish in dummy store
    return html_div([dcc_store(; id="$pre-query", data=false),
                     dcc_store(; id="$pre-query-dummy")])
end

function makelayout_backbtn(::AbstractPage)
    style = (float="right", padding="20px")
    return html_div(dcc_markdown("[BACK](..)"); style)
end

# example: reuse generic `makelayout_graph`, but enable scroll zoom
function makelayout_graph(pg::AbstractPageScatter)
    return makelayout_graph(pg; scrollZoom=true)
end
