function makelayout(pg::AbstractPageHome)
    _name(pg::AbstractPage) = last(split(string(typeof(pg)), "."))
    items = html_div([dcc_markdown("- [$(_name(pg))]($(getroute(".", pg)))")
                      for pg in pg.pages])
    return html_div([html_h1("Home page"), items])
end
