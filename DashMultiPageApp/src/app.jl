const external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

"""
Instantiate dash app and setup root and page layouts and callbacks
"""
function makeapp(pages::AbstractPage...;
                 app_title="DashMultiPageApp example",
                 url_base_pathname="/",
                 kwargs...)
    # we must set this to `true` as we will be
    # defining callbacks for all the pages
    # before they become part of the layout
    suppress_callback_exceptions = true
    # make the app's callback execution order
    # more predictable!
    prevent_initial_callbacks = true
    # instantiate dash app and set the app title
    app = dash(; suppress_callback_exceptions,
               prevent_initial_callbacks,
               external_stylesheets,
               url_base_pathname,
               kwargs...)
    app.title = app_title

    # define "root" layout with
    app.layout = html_div([dcc_location(; id="url"),  # page URL as a component
                           html_div(; id="content")]) # page content goes here

    # first check that the page routes are unique
    routes = collect(map(pg -> pg.route, pages))
    @assert unique(routes) == routes "Page routes are not unique!"
    # initialize route-to-page maps
    route2page = Dict{String,AbstractPage}()
    # instantiate the home page
    route2page[url_base_pathname] = PageHome(; pages)
    # instantiate the pages from the page specs
    for pg in pages
        route2page[getroute(url_base_pathname, pg)] = pg
    end

    # setup the root callback that fills in the page
    # given the `pathname` (aka the route)
    callback!(app,
              Output("content", "children"),
              Input("url", "pathname")) do pathname
        @cbinfo "=>content.children" pathname
        npath = normpath(pathname * "/")
        return if haskey(route2page, npath)
            makelayout(route2page[npath])
        else
            404
        end
    end

    # setup the page callbacks
    for pg in values(route2page)
        addcallbacks!(app, pg)
    end

    return app
end

"""
Instantiate and run the DashMultiPageApp dash app!

Accept all `Dash.dash` kwargs except for
`suppress_callback_exceptions` (set to true)
`prevent_initial_callbacks` (set to true) and
`external_stylesheets`

Implements addition kwarg `app_title::String` to
set the app's document title.

Examples:

runapp(8010, PageScatter())

runapp(8011, PageScatter(); debug=false, app_title="DEBUGGING")

runapp(8012, PageScatter(), PageSplom(); url_base_pathname="/app/", update_title="please wait ... ...")

runapp(8013, PageSplom(;route="one"), PageSplom(;route="two", markersize=2))

"""
function runapp(port::Int,
                pages::AbstractPage...;
                debug=false,
                kwargs...)
    app = makeapp(pages...; kwargs...)
    return run_server(app, "0.0.0.0", port; debug)
end

export runapp
