using Test

using DashMultiPageApp
using DashMultiPageApp: makeapp

using DashMultiPageApp.Dash

@testset "makeapp" begin
    @testset "various call signatures" begin
        function _check(app)
            begin
                @test app isa Dash.DashApp
                @test iszero(sum(isnothing.([app.title, app.config, app.layout,
                                             app.callbacks])))
            end
        end
        _check(makeapp(PageScatter()))
        _check(makeapp(PageScatter(); app_title="DEBUGGING"))
        _check(makeapp(PageScatter(), PageSplom(); url_base_pathname="/app/",
                       update_title="..."))
        _check(makeapp(PageSplom(; route="one"), PageSplom(; route="two", markersize=2)))
    end
    @testset "should error when pages have same route" begin
        @test_throws AssertionError makeapp(PageScatter(), PageScatter())
        @test_throws AssertionError makeapp(PageScatter(; route="a"),
                                            PageSplom(; route="a"))
    end
end
