# N.B. cannot define struct with same name as in callbacks/page_scatter.jl
pg_scatter0 = PageScatter()
Base.@kwdef struct ScatterMock <: AbstractPageScatter
    route::String = "mock-scatter"
    tracecolor = pg_scatter0.tracecolor
end

@testset "PageScatter parsehref" begin
    pg = ScatterMock()
    @test parsehref(pg, "https://a.com") == ([], nothing, nothing)
    @test parsehref(pg, "https://a.com?notgonna=work") == ([], nothing, nothing)
    query = "species=%5B\"versicolor\"%5D&x=SepalLength&y=PetalLength"
    @test parsehref(pg, "https://a.com?$query") ==
          (["versicolor"], "SepalLength", "PetalLength")

    @testset "bad queries" begin
        expectation = ([], nothing, nothing)
        @test parsehref(pg, "?species=%5B\"versicolor\"%5") == expectation
        @test parsehref(pg, "?species=%5B\"versicolor\"") == expectation
    end
end

@testset "PageScatter makefig" begin
    pg = ScatterMock()
    fig = makefig(pg, nothing, nothing, nothing)
    fig = (data=[(x=[], y=[])], layout=NamedTuple())

    # note: here we mock PageScatter with two different `tracecolor`

    @testset "with dflt `tracecolor`" begin
        pg = ScatterMock()
        fig = makefig(pg, ["versicolor"], "SepalLength", "PetalLength")
        @test map(t -> t.marker.color, fig.data) == ["blue"]
    end
    @testset "with custom `tracecolor`" begin
        pg = ScatterMock(; tracecolor=["yellow", "cyan", "magenta"])
        fig = makefig(pg, ["versicolor"], "SepalLength", "PetalLength")
        @test map(t -> t.marker.color, fig.data) == ["yellow"]
    end
end

@testset "PageScatter makequery" begin
    pg = ScatterMock()
    @test makequery(pg, nothing, nothing, nothing) == ""
    @test makequery(pg, [], "SepalLength", "PetalLength") == ""
    @test makequery(pg, ["versicolor"], "SepalLength", "PetalLength") ==
          "?species=%5B%22versicolor%22%5D&x=SepalLength&y=PetalLength"
end
