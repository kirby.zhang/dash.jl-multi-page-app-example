using Test

using DashMultiPageApp
using DashMultiPageApp: AbstractPageScatter, AbstractPageSplom,
                        parsehref, makefig, makequery,
                        parseseldata, maketabledata

include("page_scatter.jl")
include("page_splom.jl")
