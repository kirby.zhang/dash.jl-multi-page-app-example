Base.@kwdef struct MockPageSplom <: AbstractPageSplom
    route::String = "mock-splom"
end

@testset "PageSplom callbacks" begin
    pg = MockPageSplom()
    app = mockapp(pg)
    pre = prefix(pg)

    @testset "=>pts" begin
        fn = getcbfn(app, "$pre-pts.data")
        DashMultiPageApp.parsehref(::MockPageSplom, ::String) = "dummy!"
        @test fn(0, "whatever") == "dummy!"
    end

    # note: the following suite is a bit more interesting,
    # here we test the behaviour for different `triggered` outputs
    @testset "=>" begin
        fn = getcbfn(app,
                     "$pre-graph.figure",
                     "$pre-table.data",
                     "$pre-query.data")
        pts = [1, 2, 3]
        seldata = (points=[(curveNumber=1, pointNumber=1)],)
        function DashMultiPageApp.parseseldata(::MockPageSplom, seldata)
            return seldata.points[1].pointNumber
        end
        DashMultiPageApp.makefig(::MockPageSplom, pts) = ("fig", pts)
        DashMultiPageApp.maketabledata(::MockPageSplom, pts) = ("table", pts)
        DashMultiPageApp.makequery(::MockPageSplom, pts) = string(pts)
        @testset "on params.data changes" begin
            DashMultiPageApp.triggered(::MockPageSplom) = "$pre-pts.data"
            @test fn(pts, seldata, 1) ==
                  (("fig", [1, 2, 3]), ("table", [1, 2, 3]), "[1, 2, 3]")
        end
        @testset "on btn-clear clicks" begin
            DashMultiPageApp.triggered(::MockPageSplom) = "$pre-btn-clear.n_clicks"
            @test fn(pts, seldata, 1) == (("fig", []), ("table", []), "Any[]")
        end
        @testset "on selectedData relays" begin
            DashMultiPageApp.triggered(::MockPageSplom) = "$pre-graph.selectedData"
            @test fn(pts, seldata, 1) == (("fig", 1), ("table", 1), "1")
        end
    end
end
