using Test

using DashMultiPageApp
using DashMultiPageApp: prefix,
                        makelayout, addcallbacks!,
                        AbstractPage, AbstractPageScatter, AbstractPageSplom

using DashMultiPageApp.Dash

function mockapp(pg::AbstractPage)
    app = dash()
    app.layout = makelayout(pg)
    addcallbacks!(app, pg)
    return app
end

function getcbfn(app::Dash.DashApp, outid::String)
    return app.callbacks[Symbol(outid)].func
end
function getcbfn(app::Dash.DashApp, outids...)
    return getcbfn(app, "..$(join(outids, "..."))..")
end

include("page_scatter.jl")
include("page_splom.jl")
