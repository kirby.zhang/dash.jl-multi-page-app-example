using Pkg
Pkg.activate(joinpath(@__DIR__, "..", "test"))

using JuliaFormatter: format

format(joinpath(@__DIR__, ".."))
