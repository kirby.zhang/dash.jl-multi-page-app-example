using Pkg
Pkg.activate(joinpath(@__DIR__, "env", "devrepl"))

include("_watchapp.jl")

using DashMultiPageApp

# Turn on `@cbinfo` logging
using Logging
global_logger(ConsoleLogger(stdout, LogLevel_CBINFO))

function _watch()
    watch([DashMultiPageApp], "0.0.0.0", 9002) do
        tracecolor = ["#e41a1c", "#377eb8", "#4daf4a"]
        markersize = 10
        return DashMultiPageApp.makeapp(PageScatter(; tracecolor),
                                        PageSplom(; markersize);
                                        app_title="Debugging DashMultiPageApp.jl")
    end
end

_watch()

# could also be used with `@async` to get access to the REPL prompt
# between computations, but this makes the Infiltrator prompt fail
# probably a side-effect of https://github.com/JuliaDebug/Infiltrator.jl/issues/46
#
#  task = @async _watch()
